package com.wtmcodex.KotlinCleanSample.features

import android.content.Context
import android.content.res.Configuration
import com.wtmcodex.KotlinCleanSample.R
import com.wtmcodex.KotlinCleanSample.core.LanguageManager
import com.wtmcodex.KotlinCleanSample.core.di.DaggerApplicationComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import javax.inject.Inject


class WTMCodexApplication : DaggerApplication() {
    // fields
    @Inject
    lateinit var mLanguageManager: LanguageManager

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerApplicationComponent.builder().application(this).build();
    }

    override fun onCreate() {
        super.onCreate()
        initializeApplication()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        mLanguageManager.initialize(baseContext)
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        mLanguageManager = LanguageManager(this)
    }

    fun initializeApplication() {
        setDefaultFont();
    }

    // initialize default fonts for application
    fun setDefaultFont() {
    //    CalligraphyConfig.initDefault(CalligraphyConfig.Builder()
     //           .setDefaultFontPath("fonts/Roboto-Regular.ttf")
     //           .setFontAttrId(R.attr.fontPath)
     //           .build())
    }


}